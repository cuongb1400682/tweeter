//
//  StringUtilsTest.swift
//  TweeterTests
//
//  Created by Cuong Nguyen on 10/5/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import XCTest
import Tweeter

class StringUtilsTest: XCTestCase {

    override func setUp() {
        
    }

    override func tearDown() {
        
    }
    
    func testSplitMessage() {
        let messages = [
            "Word Word Word Word Word Word Word Word Word super",
            "A less than 50 characters message.",
            "I can't believe Tweeter now supports chunking my messages, so I don't have to do it myself.",
            "averylongwordaverylongwordaverylongwordaverylongwordaverylongwordaverylongwordaverylongword",
            "",
            "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.",
            "This    string\t\thas\n\n\n\nmany\t\t\t\t     kind of     \n  \n   whitespaces"
        ]
        
        let expectedResults: [[String]?] = [
            ["Word Word Word Word Word Word Word Word Word super"],
            ["A less than 50 characters message."],
            ["1/2 I can\'t believe Tweeter now supports chunking my",
             "2/2 messages, so I don\'t have to do it myself."],
            nil,
            [],
            ["1/18 At vero eos et accusamus et iusto odio dignissimos",
             "2/18 ducimus qui blanditiis praesentium voluptatum",
             "3/18 deleniti atque corrupti quos dolores et quas",
             "4/18 molestias excepturi sint occaecati cupiditate non",
             "5/18 provident, similique sunt in culpa qui officia",
             "6/18 deserunt mollitia animi, id est laborum et",
             "7/18 dolorum fuga. Et harum quidem rerum facilis est",
             "8/18 et expedita distinctio. Nam libero tempore, cum",
             "9/18 soluta nobis est eligendi optio cumque nihil",
             "10/18 impedit quo minus id quod maxime placeat facere",
             "11/18 possimus, omnis voluptas assumenda est, omnis",
             "12/18 dolor repellendus. Temporibus autem quibusdam et",
             "13/18 aut officiis debitis aut rerum necessitatibus",
             "14/18 saepe eveniet ut et voluptates repudiandae sint",
             "15/18 et molestiae non recusandae. Itaque earum rerum",
             "16/18 hic tenetur a sapiente delectus, ut aut",
             "17/18 reiciendis voluptatibus maiores alias consequatur",
             "18/18 aut perferendis doloribus asperiores repellat."],
            ["1/2 This    string\t\thas\n\n\n\nmany\t\t\t\t     kind of",
             "2/2 whitespaces"]
        ]

        for (message, expectedResult) in zip(messages, expectedResults) {
            let result = try? splitMessage(message)
            
            XCTAssert(result == expectedResult, "Failed at: \(message)")
        }
    }
    
}
