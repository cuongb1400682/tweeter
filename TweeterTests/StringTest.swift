//
//  String.swift
//  TweeterTests
//
//  Created by Cuong Nguyen on 10/7/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import XCTest
@testable import Tweeter

class StringTest: XCTestCase {

    override func setUp() {

    }

    override func tearDown() {

    }

    func testSubstring() {
        let originalString = "The quick fox jumps over the crazy dog."
        
        XCTAssert(originalString.substring(withIn: 0 ..< 3) == "The")
        XCTAssert(originalString.substring(withIn: 10 ..< 13) == "fox")
        XCTAssert(originalString.substring(withIn: 0 ..< originalString.count) == originalString)
    }
}
