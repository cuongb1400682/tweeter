//
//  MessageBubble.swift
//  Tweeter
//
//  Created by Cuong Nguyen on 10/5/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import Foundation
import UIKit

class MessageBubble: UICollectionViewCell {
    static let reuseId = "MessageBubble"
    
    private lazy var messageWrapper: UIView = {
        let view = UIView()
        
        contentView.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            view.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
            view.topAnchor.constraint(equalTo: contentView.topAnchor),
            view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            view.widthAnchor.constraint(lessThanOrEqualToConstant: UIScreen.main.bounds.width * 0.7 + 20)
        ])
        view.backgroundColor = .messageBubbleBackground
        view.layer.cornerRadius = 20
        view.layer.masksToBounds = true

        return view
    }()
    
    public lazy var message: UILabel = {
        let label = UILabel()

        messageWrapper.addSubview(label)
        NSLayoutConstraint.activate([
            label.trailingAnchor.constraint(equalTo: messageWrapper.trailingAnchor, constant: -10),
            label.topAnchor.constraint(equalTo: messageWrapper.topAnchor, constant: 10),
            label.bottomAnchor.constraint(equalTo: messageWrapper.bottomAnchor, constant: -10),
            label.leadingAnchor.constraint(equalTo: messageWrapper.leadingAnchor, constant: 10)
        ])
        label.translatesAutoresizingMaskIntoConstraints = false
        label.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        label.textColor = .textColor
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = .systemFont(ofSize: 18)

        return label
    }()
}
