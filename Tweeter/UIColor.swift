//
//  UIColor.swift
//  Tweeter
//
//  Created by Cuong Nguyen on 10/7/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import UIKit

extension UIColor {
    static let primaryColor = UIColor(red: 21.0 / 255.0, green: 31.0 / 255.0, blue: 43.0 / 255.0, alpha: 1.0)
    static let primaryDarkColor = UIColor(red: 16.0 / 255.0, green: 24.0 / 255.0, blue: 30.0 / 255.0, alpha: 1.0)
    static let textDarkColor = UIColor(red: 106.0 / 255.0, green: 123.0 / 255.0, blue: 138.0 / 255.0, alpha: 1.0)
    static let textColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    static let messageBubbleBackground = UIColor(red: 38.0 / 255.0, green: 157.0 / 255.0, blue: 240.0 / 255.0, alpha: 1.0)
}
