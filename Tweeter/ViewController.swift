//
//  ViewController.swift
//  Tweeter
//
//  Created by Cuong Nguyen on 10/5/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    private var inputContainerHeightConstraint: NSLayoutConstraint?
    
    private var bottomHeight: CGFloat {
        return (UIApplication.shared.delegate as? AppDelegate)?.window?.safeAreaInsets.bottom ?? 0
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    private lazy var statusBar: UIView = {
        let statusBar = UIView()
        view.addSubview(statusBar)
        statusBar.backgroundColor = .primaryColor
        statusBar.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            statusBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            statusBar.topAnchor.constraint(equalTo: view.topAnchor),
            statusBar.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 40),
            statusBar.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        
        let titleLabel = UILabel()
        statusBar.addSubview(titleLabel)
        titleLabel.text = "New Messages"
        titleLabel.textColor = .textColor
        titleLabel.font = UIFont.systemFont(ofSize: 19, weight: .heavy)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: statusBar.centerXAnchor),
            titleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            titleLabel.bottomAnchor.constraint(equalTo: statusBar.bottomAnchor)
        ])
    
        return statusBar
    }()
    
    public lazy var collectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: UIScreen.main.bounds,
                                              collectionViewLayout: flowLayout)

        view.addSubview(collectionView)
        collectionView.backgroundColor = .primaryDarkColor
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 8, right: 10)
        collectionView.register(MessageBubble.self,
                                forCellWithReuseIdentifier: MessageBubble.reuseId)
        collectionView.dataSource = self
        collectionView.delegate = self
        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.topAnchor.constraint(equalTo: statusBar.bottomAnchor),
            collectionView.bottomAnchor.constraint(equalTo: inputContainer.topAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        
        return collectionView
    }()
    
    private lazy var messageInput: UITextField = {
        let textField = UITextField()
        let guide = view.safeAreaLayoutGuide
        
        inputContainer.addSubview(textField)
        textField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            textField.leadingAnchor.constraint(equalTo: inputContainer.leadingAnchor, constant: 10),
            textField.topAnchor.constraint(equalTo: inputContainer.topAnchor, constant: 7),
            textField.trailingAnchor.constraint(equalTo: sendButton.leadingAnchor, constant: -10),
            textField.heightAnchor.constraint(equalToConstant: 42)
        ])
        textField.backgroundColor = .textDarkColor
        textField.layer.cornerRadius = 20
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 20))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 14, height: 20))
        textField.rightViewMode = .always
        textField.textColor = .textColor
        textField.font = .systemFont(ofSize: 18)
        textField.placeholder = "Start a message"
        textField.addTarget(self, action: #selector(textFieldChanging(_:)), for: .editingChanged)

        return textField
    }()
    
    private lazy var sendButton: UIButton = {
        let button = UIButton()

        inputContainer.addSubview(button)
        button.setTitle("Send", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.setTitleColor(.gray, for: .disabled)
        button.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(equalTo: inputContainer.topAnchor, constant: 7),
            button.trailingAnchor.constraint(equalTo: inputContainer.trailingAnchor, constant: -20),
            button.heightAnchor.constraint(equalToConstant: 40),
            button.widthAnchor.constraint(equalToConstant: 50),
        ])
        button.addTarget(self, action: #selector(sendMessage(_:)), for: .touchUpInside)
        button.isEnabled = false
        return button
    }()
    
    private lazy var inputContainer: UIView = {
        let inputContainer = UIView()

        view.addSubview(inputContainer)
        inputContainer.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            inputContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            inputContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            inputContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        inputContainer.backgroundColor = .primaryColor
        
        inputContainerHeightConstraint = inputContainer.heightAnchor.constraint(equalToConstant: 50)
        inputContainerHeightConstraint?.isActive = true
        setInputContainerHeight()
        
        return inputContainer
    }()
    
    private var messages: [Message] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handlePanningGesture(_:))))

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
        
        collectionView.reloadData()
        messageInput.reloadInputViews()
        inputContainer.reloadInputViews()
    }
    
    @objc func sendMessage(_ sender: UIButton) {
        guard let message = messageInput.text else { return }
        
        guard let subMessages = try? splitMessage(message) else {
            let alertVC = UIAlertController(title: "Error",
                                            message: "All words must not be longer than 50 characters.",
                                            preferredStyle: .alert)
            
            alertVC.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
            present(alertVC, animated: true, completion: nil)
            messageInput.text = nil
            return
        }

        messages.append(contentsOf: subMessages.map {
            Message(content: $0, isDivided: subMessages.count > 1)
        })
        messageInput.text = nil
        sendButton.isEnabled = false
        collectionView.reloadData()
        collectionView.scrollToItem(at: IndexPath(row: messages.count - 1, section: 0),
                                    at: .bottom,
                                    animated: true)
    }

    @objc func handlePanningGesture(_ gesture: UIPanGestureRecognizer) {
        let touchPoint = gesture.location(in: view)
        
        if touchPoint.y > inputContainer.frame.minY {
            messageInput.resignFirstResponder()
        }
        
        gesture.setTranslation(.zero, in: view)
    }
    
    @objc func textFieldChanging(_ textField: UITextField) {
        sendButton.isEnabled = !(textField.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
    
}

extension ViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MessageBubble.reuseId, for: indexPath)
        
        if let bubbleCell = cell as? MessageBubble {
            bubbleCell.message.text = messages[indexPath.row].displayableContent
        }
        
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize(width: UIScreen.main.bounds.width * 0.7,
                          height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let height = NSString(
                string: messages[indexPath.row].displayableContent
            )
            .boundingRect(with: size,
                          options: options,
                          attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18)],
                          context: nil).height
        
        size.height = height + 20
        size.width = UIScreen.main.bounds.size.width
        return size
    }
    
}

extension ViewController {

    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            setInputContainerHeight(CGFloat(keyboardFrame.height))
        }
    }

    @objc func keyboardWillHide(_ notification: Notification) {
        setInputContainerHeight()
    }
    
    func setInputContainerHeight(_ keyboardHeight: CGFloat = -1) {
        UIView.animate(withDuration: 0.3, animations: {
            self.inputContainerHeightConstraint?.constant = 54 + (keyboardHeight == -1 ? self.bottomHeight : keyboardHeight)
        }, completion: { _ -> Void in
            DispatchQueue.main.async {
                self.view.updateConstraints()
            }
        })
    }

}
