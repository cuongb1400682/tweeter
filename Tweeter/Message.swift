//
//  Message.swift
//  Tweeter
//
//  Created by Cuong Nguyen on 10/6/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import Foundation

struct Message {
    public var content: String = ""
    public var isDivided: Bool = false
    
    public var displayableContent: String {
        if isDivided, let firstSpaceIndex = content.firstIndex(where: { $0.isWhitespace }) {
            return String(content[firstSpaceIndex ..< content.endIndex])
        }
        
        return content
    }
}
