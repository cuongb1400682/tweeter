//
//  Utilities.swift
//  Tweeter
//
//  Created by Cuong Nguyen on 10/5/19.
//  Copyright © 2019 Cuong Nguyen. All rights reserved.
//

import Foundation

public enum UtilitiesError: Error {
    case messageTooLong
}

public func splitMessage(_ message: String) throws -> [String]  {
    let maximumLength = 50
    var messages: [String] = []
    var lastSpaceIndex = 0
    var prev = 0
    
    for i in 0 ..< message.count {
        if i - prev > maximumLength {
            throw UtilitiesError.messageTooLong
        }
        
        if message[message.index(from: i)] == " " {
            if i - lastSpaceIndex > maximumLength {
                messages.append(message.substring(withIn: lastSpaceIndex ..< prev))
                lastSpaceIndex = prev
            }
            
            prev = i
        }
    }
    
    messages.append(message.substring(withIn: lastSpaceIndex ..< message.count))
    messages = messages
        .map { $0.trimmingCharacters(in: .whitespacesAndNewlines) }
        .filter { !$0.isEmpty }
    
    return messages
        .enumerated()
        .map { (messages.count < 2) ? $1 : "\($0 + 1)/\(messages.count) \($1)" }
}
